% vim:ft=tex:
%

\chapter{Conventional Speech-to-speech Translation}
\label{cha:conventional_speech_to_speech_translation}

  \section{Overview}
  \label{sec:overview_conventional_s2s}
  The conventional speech translation system (S2S) \cite{s2s_survey} consists
  of 3 main components, automatic speech recognition (ASR), machine translation
  (MT), and text-to-speech synthesis (TTS).  As illustrated in
  Figure~\ref{fig:conv_s2s}, the work-flow of the S2S system can be described as follows.
  First, the ASR module transcribes an audio from a source language
  into a transcription. After that, the MT module translates the transcribed text
  into a target language. Finally, the TTS module synthesizes an audio given the
  target sentence. In the following sections, we give a short description for
  each component.

  \begin{figure}[ht]
    \centering
    \includegraphics[height=7cm]{conventional_s2s/conventional_s2s}
    \caption{Conventional speech translation system}
    \label{fig:conv_s2s}
  \end{figure}

  \section{Automatic Speech Recognition (ASR)}
  \label{sec:automatic_speech_recognition_asr}
  ASR aims to convert the speech signal into the corresponding word sequence.
  Let's first take a look at how it works.
  The first step of the speech recognition is to extract speech features
  $\vm o$ given speech signals $\vm x$. The standard speech feature set
  is mel-frequency cepstral coefficients (MFCCs). The MFCC features contain the components
  of the audio signals that are good for identifying the linguistic content and discard
  irrelevant information such as paralinguistic information or noise.
  After that the ASR predicts the most plausible word
  sequence consists of $K$ words $\vm w = [w_1, w_2, \cdots, w_K]$ that maximize the conditional probability,
  \begin{eqnarray}
    \hat {\vm w} &=& \argmax_{\vm w} P(\vm w| \vm o).
    \label{eq:asr}
  \end{eqnarray}
  Applying the Bayes's rule we have,
  \begin{eqnarray}
    P(\vm w|\vm o) &=& \frac{P(\vm w) P(\vm o | \vm w)}{P(\vm o)} \propto P(\vm w) P(\vm o | \vm w).
    \label{eq:asr_bayes}
  \end{eqnarray}
  As we can see, the $P(\vm w|\vm o)$ is factored into two parts. The first part is $P(\vm w)$, also called
  language model probability and the second part $P(\vm o | \vm w)$ is acoustic probability.
  %The basic sound unit is the phoneme and each language has their own phoneme set.

  The standard way to calculate $P(\vm w)$ is to use n--gram language modeling, where
  the $P(\vm w)$ is break down into smaller parts,
  \begin{equation}
    P(w=[w_1, w_2, \cdots, w_K]) = \prod_{i=1}^{K}P(w_i|w^{i-1}_{i-n+1}),
  \end{equation}
  where $P(w_i|w^{i-1}{i-n+1})$ denotes the probability of the word $w_i$ given
  $n-1$ preceding words (context words). Basically, the higher order of n--gram model
  will usually give better results. However, it is also becomes harder to calculate the
  n--gram probability for high order n--grams because of the data sparsity problem.
  The probability of the n--gram will become smaller when the order becomes higher, and
  is not helpful for the ASR system anymore.
  Usually the 3-- or 4--gram language model is adopted for the standard ASR systems.

  With regards to the acoustic model. The acoustic probability $P(\vm o|\vm w)$ is calculated by
  \begin{equation}
    P(\vm o | \vm w) = \sum_{\vm Q} P (\vm o | \vm Q)P(\vm Q | \vm w),
  \end{equation}
  where $\vm Q$ is all possible phoneme sequences derived from $\vm w$.
  As we can see, the phoneme is used instead of the word. The reason is mainly because
  we can not collect enough data for every word for a particular language.
  $P(\vm o | \vm Q)$ represents how likely it is that the speech feature $\vm o$ is
  observed given the phoneme sequence $\vm Q$ and is typically formulated by
  Gassian mixture hidden Markov model (GMM-HMM) \cite{baum1967,1162650} as illustrated in Figure~\ref{fig:hmm},
  \begin{figure}[htpb]
    \centering
    \includegraphics[width=7cm]{conventional_s2s/hmm}
    \caption{HMM-based phoneme model with 5 states. The parameter $\alpha_{ij}$ is the state transition probability
    between the state $i^{th}$ and $j^{th}$, and $\beta_i(o_k)$ is the probability that the observation
    $o_k$ is generated from the $i^{th}$ state.}
    \label{fig:hmm}
  \end{figure}

  Recently, the deep neural network (DNN) acoustic modeling has drawn much
  attention in speech recognition researchers as an alternative modeling
  technique to the GMM model. In the DNN-HMM approach, the DNN aims to
  calculate the posterior probability $\beta_i(o_k)$ instead of a mixture of Gaussians.
  The DNN-HMM outperforms the GMM-HMM when using
  the same amount of data, and in experiments in \cite{asr_dnn} it is required about 7 times larger amount of
  training data for the GMM-HMM to have the same performance as the DNN-HMM.

  %is typically formulated by using
  %mixture Gaussian hidden Markov (GMM-HMM) model,
  %\begin{equation}
    %P(\vm o | \vm w) = P(\mathbf{q}) P(\vm o | \mathbf{q}),
  %\end{equation}
  %where $\mathbf{q}$ is the state sequence that associates with word sequence $\vm w$.

  %Developing a robust ASR is not an easy task. There are two main key things
  %that would affect to the ASR performance. They are speech modeling methods
  %and the amount of training data.

  %\begin{equation}
    %P(\X|\W,\M) = \sum_q^N P(q|\X) P(\X|q),
  %\end{equation}
  %where $N$ is number of the HMM state that associated with the sentence $\W$, $P(q|\X)$ is the
  %state transition probability, and $P(\X|q)$ is the emission probability.

  %The performance of an ASR system is measured by using the word error rate (WER) metric,
  %\begin{equation}
    %WER = \frac{S + I + D}{S + I + D + C},
  %\end{equation}
  %where $C$ is number of correct words, $S$, $I$, and $D$ is the word substitution, insertion, and deletion
  %error, respectively. Because the ASR is the first component of the S2S system,
  %so all the errors from the ASR will propagate to all other components
  %and leads to a incorrect target speech. It also affect to
  %the emphasis estimation and translation as well, which will be discussed in detail in Section
  %[Ref section here].
  \section{Statistical Machine Translation (MT)}
  \label{sec:statistical_machine_translation_mt}
  The MT system lies in the middle of the S2S system, and has a job to
  translate the hypothesis from ASR module to a particular target language.
  There are many methods that can be applied to MT task such as tree-based \cite{neubig13acldemo}
  and phrase-based \cite{Koehn:2010:SMT:1734086} translation models.
  This section gives a high-level description for the phrase-based translation
  model.

  The phrase-based model uses the translation of phrases as atomic units. The idea
  is to split a sentence into small phrases and performs the translation as
  illustrated in Figure~\ref{fig:egs_phrase}.

  The MT system can be descibed in mathematics as follows. Given a source language sentence $\bold f$, the task is to find
  the best target language sentence $\bold e$ by applying Bayes's rule as follows,
  \begin{equation}
    \bold {\hat e }= \argmax_{\bold e} P(\bold e| \bold f) \\
    = \argmax_{\bold e} P(\bold e) P(\bold f | \bold e),
  \end{equation}
  where the $P(\bold e)$ is the language model for the target sentence $\bold e$,
  and the $P(\bold f | \bold e)$ is decomposed into
  \begin{equation}
    P(\bold f | \bold e) = \prod_{i=1}^{I}\phi(f_i|e_i)d_i,
  \end{equation}
  where $I$ is number of phrases in source sentence, $d_i$ is the distance score which is
  calculated by a distance-based reordering model \cite{Koehn:2010:SMT:1734086}, and $\phi(f_i|e_i)$ is the
  phrase translation probability
  \begin{equation}
    \phi(f_i|e_i) = \frac{count(e_i,f_i)}{\sum_{f'_i} count(e_i, f'_i)}.
  \end{equation}
  The phrase is extracted from a word alignment which is the output of unsupervised
  learning methods \cite{och03:asc, Neubig:2011:UMJ:2002472.2002553}.

  The advantages of phrase-based model is it can handle non-compositional phrases, and
  the more data we have, the longer phrases can be learned. The phrase-based translation model is also
  a successful approach, currently used by many big companies.
  This thesis utilized the phrase-based model for the speech translation system.
  %An example is shown in Figure~\ref{fig:egs_phrase}.

  \begin{figure}[ht]
    \centering
    \includegraphics[width=6cm]{conventional_s2s/mt}
    \caption{Example of phrase-based translation model}
    \label{fig:egs_phrase}
  \end{figure}

  \section{Text-to-speech Synthesis (TTS)}
  \label{sec:text_to_speech_synthesis_tts}
  Text-to-speech is the last component in the S2S system,
  which synthesize the target audio given the translated hypothesis.

  The most two common TTS techniques are unit selection \cite{541110} and hidden semi-Markov
  model (HSMM) based \cite{Zen:2007:HSM:1522247.1522251}.  In the unit selection method, the synthetic speech is created
  by concatenating the pieces of recorded speech extracted from a speech
  database. For example, the speech sound of ``hh'', ``ah'', ``l'', and ``ow''
  are concatenated to form the word ``hello''.  As a result, the synthetic
  speech is very natural and intelligible.  However, this method requires a
  very large speech corpus to achieve a good performance, and it is
  also difficult to control or modify the synthetic speech (e.g., increase the
  duration of particular words, or change the speaking style) \cite{Eide04acorpus-based,Black03unitselection}.  On the other hand, the HSMM-based method takes a very
  different approach to model and synthesise the speech with hidden semi-Markov
  models. It also allows us to modify the synthetic speech easily and the amount of
  required training data is also much smaller, say an hour.  It is also
  possible to rapidly adapt an existing TTS model to a particular speaker using
  a limited amount of that speaker's training data \cite{Yamagishi:2009:ASA:2209793.2210008}
  which can not be done in unit selection methods.

  The HSMM is a hidden Markov model (HMM) with explicit state duration probability
  distributions as shown in Figure~\ref{fig:conventional_s2s_hsmm},
  \begin{figure}[htpb]
    \centering
    \includegraphics[width=0.5\linewidth]{conventional_s2s/hsmm}
    \caption{An example of hidden semi-Markov models with 5 states. $\alpha_{ij}$ is
  the state transition probability from state $i$ to state $j$, $\beta_i(.)$ is
  the likelihood probability, and $p'_i(.)$ is the duration probability distribution
  for state $i$.}
    \label{fig:conventional_s2s_hsmm}
  \end{figure}
  which improves the naturalness of the synthetic speech over
  the HMM model.
  %An overview of the training and testing phrase of an HSMM-based TTS system is shown in
  %Figure~\ref{fig:hmm_tts}.
  %\begin{figure}
    %\centering
    %\includegraphics[width=10cm]{conventional_s2s/tts}
    %\caption{Overview of HSMM-based TTS system.}
    %\label{fig:hmm_tts}
  %\end{figure}
  The model training consists of three steps, label analysis, speech parameter
  extraction, and HMM training.
  %One of the interesting part of HSMM-based TTS is the label analysis.
  The label analysis converts the word sequence into full context labels which
  represent many linguistic aspects (e.g., phone identity, accent, stress,
  location, and part-of-speech).  This information makes it possible to model and
  synthesize audio more naturally. The second step is the speech parameter
  extraction which extracts acoustic parameters from speech signals.  This
  process is different from ASR module in the way that it keeps all the information
  such as duration, speaker characteristic, emphasis.

  Let's define the TTS in mathematics. The output speech parameter vector sequence $\vm o$
  %\vm o^{(t)}  = [ \vm{o}_1^\top, \cdots, \vm{o}_t^\top, \dots, \vm{o}_T^\top]^\top$
  is determined by maximizing the likelihood function given the state sequence consists
  of $T$ states $\vm q = [q_1, \cdots, q_T]$,
  and the HSMM model set $\M$

  \begin{equation}
      \vm{\hat o} = \argmax_{\fvm{o}} P(\vm W\vm{o}|\vm{q}, \M),
  \end{equation}
  where $\vm W$  is the weighting matrix for calculating the dynamic features \cite{861820}.

  This thesis adopts the HSMM-based TTS model. The reason is not only to inherit the
  advantages of the HSMM-based method, but also the flexibility to modify it to model the
  emphasized speech which is described in Section \ref{sec:emphasis_modeling}.


  %Let's first take a look at the speech parameter extraction.
  %Unlike the ASR where this process discard all the paralinguistic
  %information such as emotion, speaker characteristic, the TTS parameter
  %extraction keeps all those information.

  %Given a sentence, TTS system try to predict the speech parameter.  The most
  %successful approach for TTS is the use of hidden semi-Markov model (HSMM)
  %\cite{Zen:2007:HSM:1522247.1522251}
