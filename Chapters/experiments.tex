\chapter{Experimental Evaluations} % Main chapter title
\label{experimental_evaluation} % For referencing the chapter elsewhere, use \ref{Chapter1}

  \section{Emphasis Corpus Analysis}
  \label{sec:exp_emphasis_analysis}
    \subsection{Experiment Setup}
    \label{sub:corpus_ana_experiment_set_up}
    Evaluation was performed on the digit emphasized speech corpus, and
    conversation bilingual emphasized speech corpus \cite{truong14ococosda} as
    described in Section~\ref{Corpus_collection}. The digit corpus consists of 500 utterances spoken
    by one bilingual English-Japanese speaker. The conversation corpus has
    1015 utterances, and is spoken by 3 bilingual speakers,
    6 monolingual Japanese, and one monolingual American speakers.

    \subsection{Power and Duration Analysis}
    \label{sub:exp_power_analysis}
    In this experiment, we examine the difference of power and duration between
    normal and emphasized words. It is important to investigate the way people
    emphasise words in individual languages and also across languages. The purpose
    is to understand the way people emphasise words, then develop the emphasis
    translation in a natural way.
    %This experiment is conducted
    %using 1015 parallel sentences spoken by 2 bilingual speakers, one American
    %and one Japanese.

    In order to extract the information of power and duration. We first perform
    automatic speech alignment on the whole corpus to
    obtain the timing information for every words. Then, based on the timing
    information, we compute the amplitude information. Figure
    \ref{fig:dur_dis_digit} and \ref{fig:dur_dis_conver} show the duration
    distribution for both corpora.  We can see that the emphasized words has
    longer duration than the normal words.  However, the Japanese speakers tend
    to use less duration than the English speakers.  This is one of the Japanese
    characteristics, if the Japanese speakers emphasise words by stretching out the
    duration too much, the meaning of the words might change completely.
    This is consistent with previous observations in
    \cite{japan_prosodic}.

    Figure~\ref{fig:amplitude_conversation} show
    the amplitude distribution of normal and emphasized words for both English
    and Japanese.  We can see clearly that the emphasized words has higher
    amplitude than the normal words. We also observed that there is very a
    similar distribution for both languages. We hypothesize that the reason is in our recording
    condition for conversation corpus, the speakers are asked to speak the
    Japanese sentences right after English sentences, so the Japanese
    emphasized words may have been influenced by the English emphasized words.

    \begin{figure}[ht]
      \centering
      \includegraphics[width=8cm]{experiments/dur_dis_digit.jpg}
      \caption{Duration distribution of digit corpus}
      \label{fig:dur_dis_digit}
    \end{figure}

    \begin{figure}[ht]
      \centering
      \includegraphics[width=8cm]{experiments/dur_dis_conver.jpg}
      \caption{Duration distribution of conversation corpus}
      \label{fig:dur_dis_conver}
    \end{figure}

    \begin{figure}[ht]
      \centering
      \includegraphics[width=8cm]{experiments/power_dis.jpg}
      \caption{Amplitude distribution of conversation corpus}
      \label{fig:amplitude_conversation}
    \end{figure}

    \subsection{Pause Analysis}
    \label{sub:exp_pause_analysis}
    Pause is also one of important factor, which is used when emphasizing
    words.  However, different languages use pauses in different ways. It is also
    important for text-to-speech synthesis systems to synthesize the emphasized
    speech considering pause inserted. In this experiment, we investigate the
    use of pause in English and Japanese. The experiment was conducted using
    the conversation corpus.

    In order to analyze the pause insertion, we first detect the pauses
    automatically by using the audio forced alignment. Every non-speech segment
    that has duration higher than 50 milliseconds is considered as a pause.
    We analysed 3 positions that pauses might be inserted: before, after, and
    both sides of emphasized words.  Figure~\ref{fig:pause_analysis} show the
    correlation of position and number of pause insertion.  First, we can see
    that most of the pauses are inserted after the emphasized words in both
    languages.  Very few emphasized words have pauses inserted on both sides.
    Second, in all positions, the number of pause used by Japanese is more than
    2 times more compared to English. This indicating that Japanese subjects use many pauses
    to emphasized words.  This is important observation, we can use this
    knowledge to build a emphasized speech text-to-speech system consider pause
    for each language in a natural way.

    \begin{figure}[ht!]
      \centering
      \includegraphics[width=12cm]{experiments/pause_analysis}
      \caption{Pause inserted distribution}
      \label{fig:pause_analysis}
    \end{figure}
  \section{Emphasis Modeling}
  \label{sec:exp_emphasis_modeling}
    \subsection{Experimental Setup}
    \label{sub:experimental_setup_emphasis_modeling}
  Experiments were performed using conversation corpus.
  The original corpus has 1015 sentences. After filtering out long utterances
  over 10 words which are not good for model training, we obtained 966 utterances,
  which we divided into 916 utterances with 1,186 emphasized words for training and
  50 sentences with 62 emphasized words for testing.
  The speech features were extracted using 25 dimensions mel-cepstral coefficients
  including spectral parameters, log-scaled $F_0$, and aperiodic features.
  Each speech parameter vector included the static features and their delta
  and delta-deltas. The frame shift was set to 5 ms. Each full contextual label
  is modeled by 7 HMM states including initial and final states. We also adopt
  STRAIGHT \cite{straight_Kawahara1999187} for speech analysis.

  In order to measure the relationship of the word-level emphasis across languages, we
  calculate the Pearson correlation coefficient to measure the strength of the linear association
  between them,
    \begin{equation}
        r = \frac{\sum_i(\lambda_i^{(en)} - \bar\lambda^{(en)})(\lambda_i^{(ja)} - \bar\lambda^{(ja)})}{\sqrt{\sum_i{(\lambda_i^{(en)} - \bar\lambda^{(en)})}^2}\sqrt{\sum_i{(\lambda_i^{(ja)} - \bar\lambda^{(ja)})}^2}},
    \end{equation}
    where $r$ is the Pearson correlation coefficient, $\lambda_i^{(en)}$ is the emphasis level
    for the word $i$-th in English and $\lambda_i^{(ja)}$ is the emphasis level for the
    corresponding Japanese word which is determined by one-to-one word alignment,
    $\bar \lambda^{(en)}$ and $\bar \lambda^{(ja)}$
    is the mean of emphasis level of English and Japanese, respectively.

    \subsection{Word-level Emphasis Estimation Evaluation}
    \label{sub:exp_emphasis_estimation_evalutation}
    First, we validate that the proposed method is able to detect emphasis and
    find which acoustic features (spectral, F0, duration) are more useful to
    estimate the emphasis or distinguish between the emphasized and normal
    words. We do so by optimizing the emphasis weight sequence using different
    settings of acoustic features:
    \begin{itemize}
    \item \textbf{dur}: using only the duration feature.
    \item \textbf{lf0}: using only log $F0$ (lf0) feature.
    \item \textbf{sp}: using only spectral features.
    \item \textbf{sp\_dur}: using spectral and duration features.
    \item \textbf{sp\_lf0}: using spectral and lf0 features.
    \item \textbf{lf0\_dur}: using lf0 and duration features.
    \item \textbf{sp\_lf0\_dur}: combine all features.
    \end{itemize}

    The estimated word-level
    emphasis is then classified into labels of 0 and 1 indicating normal and
    emphasized words by using a emphasis threshold of 0.5. Then, we calculate the
    $F$-measure to show how accurate the system can detect the emphasis. The
    process is illustrated in Figure~\ref{fig:emph_est_process}. The result is
    shown in Figure~\ref{fig:emph_est_eval}.

    Looking at the duration column, we can see that the duration feature
    works not so bad in English, but does not work well in Japanese. We can
    also observed this situation when combine lf0-duration or mgc-duration, the
    performance did not increase significantly compared to lf0 or mgc only.
    This problem is caused by the characteristic Japanese where people can not use long duration
    to emphasise words as the meaning of the words might change if their duration
    changes.
    The result also showed that in English
    all three feature duration, $F_0$, and spectral play the same role in term of emphasis
    prediction because they gave fairly equal performance. However, for Japanese, the spectral feature is more significant
    compared to the other two.

    By combining all features together. We achieved the best performance for both languages.
    The $F$-measure for English is 75.63\% and Japanese is 80.36\%. Therefore, we will
    use this combination for the emphasis translation experiments.

    \begin{figure}[ht]
      \centering
      \includegraphics[width=8cm]{experiments/emph_est_process}
      \caption{Word-level emphasis estimation evaluation process.}
      \label{fig:emph_est_process}
    \end{figure}

    \begin{figure}[ht]
      \centering
      \includegraphics[width=14cm]{experiments/emph_est_eval}
      \caption{$F$-measure of emphasis prediction.}
      \label{fig:emph_est_eval}
    \end{figure}

    \subsection{Analysis of Emphasis across English and Japanese}
    \label{sub:exp_analysis_of_emphasis_across_english_and_japanese}
    In this experiment, we investigated the relationship of the word-level emphasis between
    English and Japanese. The result might be helpful to construct the emphasis translation
    system. The experiment was performed using conversation corpus.

    %The Pearson correlation coefficient to measure the
    %strength of the linear association between them,
    Figure~\ref{fig:pearson_coefficient} shows a scatter plot of the relationship
    with the Pearson correlation coefficient is 0.625, indicating that there is some
    correlation of emphasis level between two languages, but it is not so high.
    It might possible to develop a emphasis translation by using a linear function. However,
    a more sophisticated method should be applied since the correlation coefficient is not high.

    \begin{figure}[ht]
      \centering
      \includegraphics[width=12cm]{experiments/pearson_coefficient}
      \caption{Relationship between English and Japanese word-level emphasis}
      \label{fig:pearson_coefficient}
    \end{figure}

    \subsection{Effect of ASR error on the emphasis estimation}
    \label{sub:exp_effect_of_asr_error_on_emphasis_estimation}
    In this experiment, we investigated the effect of ASR errors on emphasis estimation.
    The reason why we conducted this experiment is to investigate
    the effect of 3 different ASR errors: deletion, insertion, and substitution
    on the emphasis estimation. By knowing those effect, we can adjust the ASR system in
    a way that has smallest effect on the emphasis estimation. For example, we can adjust
    the word insertion penalty of the decoding process to trade off between deletion
    and insertion error.
    %Even with the stage-of-the art ASR system based on deep neural network,
    %the performance of ASR is still far from human performance.
    %Similar to MT and TTS, the emphasis estimation process will be affected by
    %the error of ASR.

    In order to conduct the experiment, we simulated 3 type of ASR errors for each
    utterance as follows,
    \begin{description}
      \item[Deletion:] For each utterance, we randomly delete one word.
      \item[Substitution:] For each utterance, we randomly substitute one word with
        another word that has similar pronunciation. This is usually the case of substitution
        error in real ASR system.
      \item[Insertion:] For each utterance, we randomly insert one word in an existing dictionary.
    \end{description}
    It should be noted that we assumed the emphasized words are recognized
    correctly or kept unchanged in our simulation.

    Then, we performed the emphasis estimation on modified transcription, and calculate the
    $F$-measure in a similar way with the previous experiment \ref{sub:exp_emphasis_estimation_evalutation}.
    The result is shown in Figure~\ref{fig:effect_asr_error}.
    As we can see, for both languages, the insertion error has largest effect on emphasis estimation,
    while the deletion and substitution errors has smallest effect to English and Japanese, respectively.
    The result indicates that when we perform the emphasis translation, we
    should adjust the ASR to produce less insertion error by increasing the word insertion penalty
    during decode process. Although it will increase the deletion error, the effect to emphasis estimation
    can be reduced.

    \begin{figure}[ht]
      \centering
      \includegraphics[width=12cm]{experiments/effect_asr_error}
      \caption{Effect of ASR errors on emphasis estimation}
      \label{fig:effect_asr_error}
    \end{figure}

    %\section{ASR and MT System Performance}
    %\label{sec:asr_and_mt_system_performance}
    %\subsection{ASR Setup}
    %\label{sub:asr_setup}
    %\subsubsection{Acoustic Model}
    %\label{ssub:Acoustic Model}

    %We adopt the state-of-the-art deep neural net (DNN) acoustic modeling for
    %ASR system. The DNN is a p-norm deep neural network \cite{6853589} with 6
    %hidden layers. The input feature for the DNN is the combination of 40
    %dimensions standard mel-frequency cepstral coefficients (MFCCs) and 100
    %dimensions intermediate vector (i-vector) features. The model parameters is
    %optimized by cross-entropy function.
    %The acoustic model is trained using a data set consisting about 700 hours.

    %\subsubsection{Language Model}
    %\label{ssub:Language Model}
    %N-gram language modeling.

    %\subsubsection{Decoding Performance}
    %\label{ssub:Decoding Performance}
    %[Add asr performance]

    %\subsection{MT Setup}
    %\label{sub:mt_setup}

  \section{Emphasis Translation}
  \label{sec:exp_emphasis_translation}
    In this section, we evaluate the performance of emphasis translation based
    on conditional random fields (CRFs). There are two emphasis translation
    systems were evaluated.  The first system is emphasis translation
    without ASR and MT. In this experiment, we assumed the ASR and MT system
    output correct hypotheses. The purpose is to evaluate the performance of
    the emphasis translation only. In the second system, we take into
    account the error of ASR and MT, and evaluate the whole translation system.

    \subsection{Experimental Setup}
    \label{sub:experimental_setup_emphasis_trans}
    The experiment was conducted using the conversation corpus.
    However, due to the limitation of the speaker dependence of the current
    emphasis estimation and synthesis, we use only 1 native English speaker and
    1 native Japanese speaker. We used 916 utterances for CRFs training for
    both experiments. For the experiment of emphasis translation without ASR
    and MT, we used 50 sentences, and for the second
    experiment, we used 95 utterances for
    testing.  The reason why the test sets are different is in the second
    experiment where we take into account the errors of ASR and MT, not all the
    utterances can be translated to the target language correctly, some of them
    lost the meaning completely. It is hard to evaluate the naturalness of the audios
    that has no meaning, and also it is not possible to evaluate the emphasis
    prediction as well. To ensure a good amount of testing data for subjective evaluation,
    we increase the number of test data for the second experiment.  For the first experiment, where
    the errors of ASR and MT are ignored, the meaning of target utterance is
    preserved, so we can evaluate the emphasis prediction for all utterances.

    Because the CRFs only deal with discrete values, we had to quantize the
    continuous word-level emphasis to the closest of {0, 0.3, 0.6, 0.9} unless
    stated otherwise, which we describe in detail in
    subsection~\ref{ssub:word_level_emphasis_quantization_evaluation}

    \subsection{Emphasis Translation Evaluation Without ASR and MT}
    \label{sub:Emphasis_Translation_Evaluation_without_asr_mt}
      \subsubsection{Emphasis prediction evaluation}
      \label{ssub:emphasis_prediction_evaluation_without_asr_mt}
        In this experiment, we evaluate the ability of the proposed method to
        reproduce emphasis in the target language where there are no errors
        form ASR and MT. In addition, we also evaluate
        the effect of the combination of input features described in subsection \ref{sub:conditional_random_fields}
        to find out which features gives the highest $F$-measure.
        The translated word-level
        emphasis is classified into binary values (1 and 0) using a threshold 0.5.
        We have evaluated different thresholds, and found out that 0.5 is the best
        value to classify emphasized and normal words. We also use a baseline ``All
        Emphasis'' that predicts every word is emphasized, this is the chance
        rate that we want to beat. Then we calculate the $F$-measure for each system.
        The result is shown in Table~\ref{tab:feature_eper}

      \begin{table}[htpb]
        \centering
        \caption{ ${F}$-measure for different combinations of input features.}
        \label{tab:feature_eper}
        \begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|}
        \hline
        \multicolumn{2}{|c}{\footnotesize {\textbf{Emphasis}}} & \multicolumn{2}{|c}{\footnotesize {\textbf{Word}}} & \multicolumn{2}{|c}{\footnotesize {\textbf{Tag}}} &
        \multicolumn{2}{|p{15mm}}{{\footnotesize \textbf{Word \newline context}}} &
        \multicolumn{2}{|p{15mm}|}{\footnotesize {\textbf{Tag \newline context}}} & \textbf{$F$-measure}  \\
        \hline
        \footnotesize En & \footnotesize  Ja & \footnotesize En & \footnotesize  Ja & \footnotesize  En & \footnotesize  Ja & \footnotesize  En & \footnotesize Ja & \footnotesize  En & \footnotesize Ja &
         \\
        \hline
        \hline
        & \cmark & & \cmark & & \cmark & & & & & 81.6 \\
        \hline
        \cmark & \cmark & & & & & & & & & 82.8 \\
        \cmark & \cmark & \cmark & \cmark & & & & & & & 84.8 \\
        \cmark & \cmark & \cmark & \cmark & \cmark & \cmark & & & & & 90.0 \\
        \cmark & \cmark & \cmark & \cmark & \cmark & \cmark & & & & \cmark & \textbf{91.6} \\
        \cmark & \cmark & \cmark & \cmark & \cmark & \cmark & \cmark & \cmark & & & 88.67 \\
        \cmark & \cmark & \cmark & \cmark & \cmark & \cmark & \cmark & \cmark & \cmark & \cmark & 90.0 \\
        \hline

        \end{tabular}
      \end{table}

        %\begin{table}
        %\centering
        %\caption{ ${F}$-measure for different combinations of input features.
        %{\em e\_en} and {\em e\_ja} denote word-level emphasis,
        %{\em w\_en} and {\em w\_ja} denote word information, and {\em t\_en} and {\em t\_ja} denote
            %PoS tag of English, and Japanese, respectively.}
        %\begin{tabular}{|l|r|}
            %\hline
            %\textbf{Feature type} & \textbf{$\vm F$-measure} (\%) \\
            %\hline
            %\hline
            %All Emphasis  & 42.5 \\
            %\hline
            %{\em{w\_ja}}, {\em{t\_ja}}, {\em{t\_ja\_c}}, {\em{e\_ja}} & 81.6 \\
            %\hline
            %{\em{e\_en}}, {\em{e\_ja}}  &  82.8 \\
            %{\em{e\_en}}, {\em{e\_ja}}, {\em{e\_en\_c}}  & 82.8 \\
            %{\em{e\_en}}, {\em{e\_ja}}, {\em{w\_en}}, {\em{w\_ja}}  & 84.8 \\
            %{\em{e\_en}}, {\em{e\_ja}}, {\em{w\_en}}, {\em{w\_ja}}, {\em{t\_en}}, {\em{t\_ja}}  & 90.0 \\
            %{\em{e\_en}}, {\em{e\_ja}}, {\em{w\_en}}, {\em{w\_ja}}, {\em{t\_en}}, {\em{t\_ja}}, {\em{t\_ja\_c}}  & \textbf{91.6} \\
            %\hline
        %\end{tabular}
        %\label{tab:feature_eper}
        %\end{table}

        The chance rate (all word-level emphasis are set to 1) has the $F$-measure of 42.5\%.
        Comparing the chance rate with the result table, we can see that the emphasis translation
        model outperforms the chance rate by approximately 40\%. This indicates that
        our proposed system can produce emphasis in the target language relatively accurately.
        Next, we can see that the
        model that use the features including the source language information (2nd-7th
        row) is better than the model use only target information (1st row). This
        demonstrates that our model is effectively translating emphasis from the source,
        as opposed to simply predicting based on the target.

        Looking at the third and fourth rows, we can see that the emphasis context in
        the source language does not help
        the word-level emphasis translation, indicating that word-level emphasis in the target language depends mainly on the emphasis of the corresponding source word. By adding
        the word information in both languages, the accuracy increased by 2\%, and
        further increased when adding the PoS tag information by approximately 6\%.
        Finally, we add the context of the PoS tags in Japanese, yielding
        the best system with 91.6\% accuracy. This is consistent with the characteristic of the corpus
        that content words are usually emphasized. We also tested with other combinations of
        the features, but none of them gave the accuracy higher than 91.6\%.
        Overall, the result indicates that along with acoustic features (emphasis
        level), the linguistic features such as word, PoS tag are also contributing to
        the improvement of the translation model.

      \subsubsection{Word-level Emphasis Quantization Evaluation}
      \label{ssub:word_level_emphasis_quantization_evaluation}
        Next, we examine the effect of word-level emphasis quantization to the
        translation model. When creating the CRFs model, we use 4 different quantization schemes.
        \begin{description}
            \item [{0/1} Quant:] The word-level emphasis is quantized into the closest of 1 and 0
            \item [{0.3 Quant:}] The word-level emphasis is quantized into the closet of
                \{0, 0.3, 0.6, 0.9\}.
            \item [{0.1 Quant:}] The word-level emphasis is quantized into bucket of 0.1
            \item [Labels:] The word-level emphasis in source language is quantized according to
               ``0.3 Quant'' and the emphasis in target language is derived from the labels
                from the corpus and has binary values, 1 for emphasis, 0 for normal.
        \end{description}
        \begin{table}
          \centering
          \caption{$F$-measure for different quantization methods.}
          \begin{tabular}{|l|r|}
              \hline
              \textbf{System} & \textbf{$\vm F$-measure} (\%) \\
              \hline
              \hline
              0/1 Quant & 85.5 \\
              0.1 Quant & 90.8 \\
              0.3 Quant & \textbf{91.6} \\
              \hline
              Labels & 90.7 \\
              \hline
          \end{tabular}
          \label{tab:quantize_eper}
        \end{table}

        From the result, we can see that the quantization scheme ``0.3 Quant'' gives the
        best result, likely because it provides an appropriate amount of training data.
        And more importantly,
        it even outperforms the manually created ``Labels,'' suggesting that training the system using
        the quantized word-level emphasis can be more effective than binary values.

      \subsubsection{Subjective Evaluation}
      \label{ssub:manual_evaluation_without_asr_mt}
        In this experiment, we performed a manual evaluation to determine
        how well the end-to-end system can translate emphasis.
        We asked 6 native Japanese speakers to listen to the emphasis translated utterances, and select the words that they think
        are emphasized in 150 randomized testing utterances
        from the following 3 systems.
        \begin{description}
            \item [{Baseline:}] No emphasis translation is performed. The TTS is trained
                using a normal decision tree.
            \item [{CRF-based:}] Emphasis is translated from English to Japanese
                using the CRF model, which is trained using the best features in Table \ref{tab:feature_eper}.
            \item [{Natural:}] Natural speech spoken speech by a Japanese speaker.
        \end{description}
        Fig. \ref{fig:subject_eval} shows
        the accuracy for all 3 systems. We can see that the proposed
        emphasis translation model achieves a large improvement over the
        baseline system by 11.8\% $F$-measure. The audio
        generated by the baseline system have many words that are randomly emphasized,
        because there is no emphasis control based on the source utterance.

        Comparing these results with the automatic evaluation,
        we can still see a gap of approximately 4\% between the results.
        This is likely due to problems of speech synthesis. When listening to the
        natural and synthetic audio, we found that there are often pauses inserted
        in natural speech in order to emphasize words, which the synthetic audio does not have.
        This problem can be addressed by introducing a pause prediction model in the target language.

        \begin{figure}[!htb]
            \centering
            \includegraphics[width=10cm]{./experiments/subjective_eval_without_asr_mt}
            \caption{Emphasis prediction $F$-measure for manual evaluation}
            \label{fig:subject_eval}
        \end{figure}

    \subsection{Emphasis Translation Evaluation with ASR and MT}
    \label{sub:exp_emphasis_translation_with_asr_and_mt}
      In order to ensure that the proposed method works well in real S2S system,
      experiments were also performed considering the ASR and MT errors.

      \subsubsection{Emphasis prediction evaluation}
      \label{ssub:emphasis_prediction_evaluation_with_asr_mt}
      One of the obstacles that we need to solve in order to perform this
      experiment is that when we take into account the ASR and MT, the
      translated transcription differs from the label that we already
      have. It is not possible to perform the objective evaluation without
      the reference. Our solution is to generated the emphasis labels from translated
      transcription automatically. We do that by performing the text alignment
      between translated transcription and original label. The process is
      illustrated in Figure~\ref{fig:label_generate}.
      \begin{figure}[ht]
        \centering
        \includegraphics[width=12cm]{experiments/label_generate}
        \caption{Translated label generation}
        \label{fig:label_generate}
      \end{figure}

      However, we can only generate the label for 89 out of 95 translated utterances.
      The other 6 utterances lost the emphasized words due to the errors from ASR and MT
      modules and are removed from the further evaluations because the meaning of those
      sentences are lost and it does not make much sense to be included in the evaluation.

      The same experiment is conducted as described in subsection
      \ref{ssub:emphasis_prediction_evaluation_without_asr_mt} to find the best
      features for CRFs translation model. Three word-level emphasis quantization
      includes ``0/1 Quant.'', ``0.1 Quant.'', and ``0.3 Quant.'' schemes are also evaluated.
      The result is shown is
      Table~\ref{tab:feature_eper_with_asr_mt}.
      As the result, the word-level emphasis context in the source language
      does not help the translation as also observed in the previous
      experiment. The result also indicates that the more information included
      in the
      translation model, the better accuracy we have. We also observed that
      when used too much information, the performance is dropped as shown in
      the last row of the result table, this might due to the model is
      over-fitting with the training data.  The feature combination of the
      emphasis level, word, word context in the source language, and the part
      of speech tagging in the target language gives the best performance.

      \begin{table}[htpb]
        \centering
        \caption{ ${F}$-measure for different combinations of input features and 3 types of
      quantization scheme (0.1 Quant., 0.3 Quant., and 0/1 Quant.).}
        \label{tab:feature_eper_with_asr_mt}
        \begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|c|c|}
        \hline
        \multicolumn{2}{|c}{\footnotesize {\textbf{Emphasis}}} & \multicolumn{2}{|c}{\footnotesize {\textbf{Word}}} & \multicolumn{2}{|c}{\footnotesize {\textbf{Tag}}} &
        \multicolumn{2}{|p{15mm}}{{\footnotesize \textbf{Word \newline context}}} &
        \multicolumn{2}{|p{15mm}|}{\footnotesize {\textbf{Tag \newline context}}} & \multicolumn{3}{c|}{\textbf{$F$-measure}}  \\
        \hline
        \footnotesize En & \footnotesize  Ja & \footnotesize En & \footnotesize  Ja & \footnotesize  En & \footnotesize  Ja & \footnotesize  En & \footnotesize Ja & \footnotesize  En & \footnotesize Ja & {\footnotesize 0.1 Quant. } & {\footnotesize 0.3 Quant. } & {\footnotesize 0/1 Quant. }
         \\
        \hline
        \hline
        \cmark & \cmark & & & & & & & & & 64.13 & 69.66 & 71.20 \\
        \cmark & \cmark & \cmark & \cmark & & & & & & & 72.63 & 72.29 & 78.57 \\
        \cmark & \cmark & \cmark & \cmark & \cmark & \cmark & & & & & 72.81 & 73.07 & 79.5 \\
        \cmark & \cmark & \cmark & \cmark & \cmark & \cmark & \cmark & \cmark & & & \textbf{80.0} & 74.39 & 79.02 \\
        \cmark & \cmark & \cmark & \cmark & \cmark & \cmark & \cmark & \cmark & \cmark & \cmark & 77.98 & 73.93 & 78.64 \\
        \hline

        \end{tabular}
      \end{table}

        %\begin{table}
        %\centering
        %\caption{ ${F}$-measure for different combinations of input features and 3 types of
        %quantization scheme (0.1 Quant., 0.3 Quant., and 0/1 Quant.).
        %{\em e\_en} and {\em e\_ja} denote word-level emphasis,
        %{\em w\_en} and {\em w\_ja} denote word information, and {\em t\_en} and {\em t\_ja} denote
            %PoS tag of English, and Japanese, respectively.}
        %\begin{tabular}{|l|c|c|c|c|}
            %\hline
            %\textbf{Feature type} & \multicolumn{3}{c|}{ \textbf{$\vm F$-measure} (\%)}  \\
            %\hline
            %& {\footnotesize 0.3 Quant.} & {\footnotesize 0.1 Quant. }& {\footnotesize 0/1 Quant.}\\

            %\hline
            %\hline
            %All Emphasis  & \multicolumn{3}{c|}{29.56} \\
            %\hline
            %\hline
            %{\em{e\_en}}, {\em{e\_ja}}  &  69.66 & 64.13 & 71.20 \\
            %{\em{e\_en}}, {\em{e\_ja}}, {\em{e\_en\_c}}  & 68.47 & 62.5 & 63.87 \\
            %{\em{e\_en}}, {\em{e\_ja}}, {\em{w\_en}}, {\em{w\_ja}}  & 72.92 & 72.63 & 78.57 \\
            %{\em{e\_en}}, {\em{e\_ja}}, {\em{w\_en}}, {\em{w\_ja}}, {\em{w\_en\_c}}, {\em{w\_ja\_c}}   & 72.82 & 73.89 & 77.55 \\
            %{\em{e\_en}}, {\em{e\_ja}}, {\em{w\_en}}, {\em{w\_ja}}, {\em{t\_en}}, {\em{t\_ja}}  & 73.07 & 72.81 & 79.6 \\
            %{\em{e\_en}}, {\em{e\_ja}}, {\em{w\_en}}, {\em{w\_ja}}, {\em{w\_en\_c}}, {\em{w\_ja\_c}}, {\em{t\_en}}, {\em{t\_ja}}  & 74.39 & \textbf{80} & 79.02 \\
            %{\em{e\_en}}, {\em{e\_ja}}, {\em{w\_en}}, {\em{w\_ja}}, {\em{w\_en\_c}}, {\em{w\_ja\_c}}, {\em{t\_en}}, {\em{t\_ja}}, {\em{t\_en\_c}}, {\em{t\_ja\_c}}  & 73.93 & 77.98 & 78.64 \\
            %\hline
        %\end{tabular}
        %\label{tab:feature_eper_with_asr_mt}
        %\end{table}
      %\subsubsection{Word-level Emphasis Quantization Evaluation}
      %\label{ssub:Word_level_Emphasis_Quantization_Evaluation_with_asr_mt}
      %We also performed the same emphasis quantization evaluation as in the previous
      %experiment. 3 quantization schemes ``0/1 Quant'', ``0.3 Quant'', and ``0.1 Quant''
      %are evaluated. The result is shown in Table~\ref{tab:quantize_eper_with_asr_mt}.
      %[Write analysis]

        %\begin{table}
          %\centering
          %\caption{$F$-measure for different quantization methods.}
          %\begin{tabular}{|l|r|}
              %\hline
              %\textbf{System} & \textbf{$\vm F$-measure} (\%) \\
              %\hline
              %\hline
              %0/1 Quant & 85.5 \\
              %0.1 Quant & 82.63 \\
              %0.3 Quant & 79.75 \\
              %\hline
          %\end{tabular}
          %\label{tab:quantize_eper_with_asr_mt}
        %\end{table}

      \subsubsection{Subjective Evaluation}
      \label{ssub:manual_evaluation_with_asr_mt}
      In this experiment, we asked 6 native Japanese listeners to listen to
      the synthesized audios from the best system above. Then, select the words
      that they think are emphasized. The $F$-measure score is calculated to evaluate
      the accuracy.
      There are two system are evaluated in this experiments,
      \begin{description}
        \item[Baseline]: No emphasis translation is performed.
        \item[0 emphasis]: All word-level emphasis are set to 0. The output of this
          system is equal to the original TTS system where there is no emphasis in
          the speech.
        \item[CRF-based]: Performs the emphasis translation using CRFs method.
      \end{description}
      Because it is not possible to obtain the same translated text
      with the original reference, so we do not have the natural voice system
      in this experiment.
      The result is shown in Table~\ref{tab:subjective_eval_with_asr_mt}. The CRF-based
      translation outperforms the baseline method. It is also dropped by about 5\%
      compared to the objective evaluation due to some problems of TTS module.
      The $F$-score and also recall of ``0 emphasis'' system is very low indicates
      that our TTS module can also produce the non-emphasized speech where the
      ``baseline'' system can not.

      \begin{table}[htpb]
        \centering
        \caption{$F$-measure of subjective evaluation.}
        \label{tab:subjective_eval_with_asr_mt}
        \begin{tabular}{|c|c|c|c|}
        \hline
        \textbf{System} & \textbf{$F$-measure} & \textbf{Recall} & \textbf{Precision} \\
        \hline
        \hline
        Baseline & 63.77 & 67.25 & 60.33 \\
        \hline
        CRF-based & \textbf{75.27} & 76.42 & 74.15 \\
        \hline
        0 emphasis & 25.15 & 17.90 & 42.27 \\
        \hline
        \end{tabular}
      \end{table}

      %\begin{figure}
      %\begin{center}
        %\includegraphics[scale=0.8]{experiments/subjective_eval_with_asr_mt}
      %\end{center}
      %\caption{$F$-score for subjective evaluation.}
      %\label{fig:subjective_with_asr_mt}
      %\end{figure}

      \subsubsection{Naturalness Evaluation}
      \label{ssub:Naturalness_Evaluation_with_asr_mt}
      In order to evaluate the naturalness of the proposed method. We performed
      preference (A/B) test for each pair of the system described in the previous
      subsection.
      Figure~\ref{fig:AB}
      \begin{figure}
        \centering
        \includegraphics[width=0.8\linewidth]{experiments/AB}
        \caption{Preference for each pair of methods from 6 listeners}
        \label{fig:AB}
      \end{figure}
       shown the result of A/B preference test for each pair of the methods.
       The preference score for each method are close to each other, indicating
       that the emphasis translation does not degrade the naturalness of the
       synthetic speech.

