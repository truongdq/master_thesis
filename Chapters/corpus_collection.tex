% Chapter 2

\chapter{Corpus Collection} % Main chapter title
\label{Corpus_collection} % For referencing the chapter elsewhere, use \ref{Chapter1}
%----------------------------------------------------------------------------------------
\section{Construction of Emphasized Speech Corpora}
    In this section, we describe the creation of emphasized utterances from
    two well-known speech corpora AURORA \cite{aurora} and BTEC \cite{btec}. The smaller AURORA corpus is suitable
    for small experiments and developing prototype systems. When the larger
    BTEC corpus can be used for experiments with larger vocabulary, and
    creation more realistic systems.  The details are as follows.
\label{sec:construction_of_a_corpus_of_emphasized_speech}
    \subsection{Original Material}
    \label{sub:original_material}
    As our parallel data, we use the AURORA corpus of digit strings and the
    BTEC corpus of travel conversation sentences.
    \begin{description}
     \item [AURORA:] AURORA \cite{aurora} is a speech corpus consisting of
    8440 connected digits strings in English. The digits are from 0 to 9. It
    has also been translated to Japanese. The length of the string
    varies from 1 to 8.

     \item [BTEC] BTEC \cite{btec} is Basic Travel Expression Corpus was
    collected by bilingual experts from Japanese-English, and their translations
    cover a wide variety of content in the travel domain. BTEC has also
    been translated into several languages including French, German, Italian,
    Chinese and Korean.
    \end{description}
    Using these corpora as a basis for our recording material reduces the
    burden of corpus construction, as we can just choose appropriate sentences
    from the sentences in the original corpus. We focused on AURORA and BTEC
    because they have the parallel sentences for English and Japanese and
    contain relatively short utterances, allowing for easier analysis.
    \begin{center}
    \begin{table}[!ht]
     \centering
     \caption{Examples of English-Japanese bilingual BTEC sentences}
    \begin{tabular*}{13cm}{p{6cm}|p{7cm}}
     \textbf{English} & \textbf{Japanese} \\
    \hline
    Could you recommend a good restaurant? & \begin{CJK}{UTF8}{min}
    どこかよいレストランを紹介してもらえませんか? \end{CJK} \\
    \hline
    Do you feel weary? & \begin{CJK}{UTF8}{min} だるいですか?\end{CJK}\\
    \end{tabular*}

    \label{tbl:btec}
    \end{table}
    \end{center}

    \subsection{Digit string corpus}
    As translation of paralinguistic information is a difficult task, it is useful
    to have a corpus with limited vocabulary to develop a prototype emphasized
    translation system. To this effect, we use the AURORA digit string corpus,
    which has a vocabulary containing only 10 digits. The lexical content of the
    corpus (digit strings) were selected from the TIDigit/AURORA2 data set.
    We selected phonetically-balanced corpus from 8440 strings training data,
    resulting 445 strings, and randomly selected 55 strings from test data.
    We chose a total 500 strings that maintain the phonetic
    balance using the greedy search algorithm \cite{greedy}.

    Because all words play the same role in the digit strings, digits can be
    emphasized randomly. For each utterance, we randomly selected one digit and
    marked it to be emphasized in the recording step.

    \subsection{Conversation Corpus}
    \label{sub:conventional_corpus}
    While the digit corpus can be used in prototype systems, it is also too simple
    to reflect the actual complexities of paralinguistic translation. In the
    digit corpus, the emphasized words are chosen randomly, and to find the
    emphasized
    digit in the target language given the emphasized digit in source language is
    easy, as digits are one-to-one translations. In normal translation tasks,
    however, we have to consider the naturalness of the emphasized sentences and
    also the correctness of the emphasis in the target speech.
    In order to overcome these problems, we construct a corpus in the manner shown
    in Figure \ref{fig:traveling_corpus_selection}.

    First, we selected 16,000 pairs of sentences from the BTEC corpus,
    and performed part-of-speech tagging on both languages.
    We used NLTK \cite{nltk} for English and Mecab \cite{mecab} for Japanese.

    Next, we performed word alignment between the sentences using the pialign tool
    \cite{pialign}. The alignment helps to determine the emphasized units in
    the target language given the emphasized units in the source language.
    In order to make this decision easily, we only keep the pairs of sentences which
    have alignments where if the emphasized word in the source language is a noun,
    the corresponding emphasized word in the target language is also a noun, and
    similarly for adjectives and adverbs.
    The example in Figure \ref{fig:traveling_corpus_selection} illustrates the
    selection of the sentences where the emphasize words are noun.
    This step is repeated for adjectives and adverbs.

    After this, we had 2500 sentences. These sentences were verified manually to
    ensure the correctness of emphasized units and the naturalness.

    After manual verification, a total of 1015 pairs of sentences remained,
    and the detail of the corpus is shown in Table \ref{tbl:travel_corpus}.
    Almost all sentences had only one emphasized unit.
    This is natural because we often emphasize the important information in the
    sentence,
    and the number of important words are often one or two. In a limited
    number of cases, we emphasized more than two words.

    \begin{figure}[!ht]
     \centering
     \centerline{\includegraphics[width=7.5cm]{corpus_collection/selection.jpg}}
    %  \vspace{2.0cm}

    \caption{Creation of emphasized sentences in the conversation corpus (nouns)}
    \label{fig:traveling_corpus_selection}
    %
    \end{figure}
    \begin{center}
    \begin{table}[!ht]
     \centering
     \caption{The conversation corpus materials}
    \begin{tabular}{l l|l}
    \multicolumn{2}{l|}{\textbf{Utterances}}&1015\\
    \hline
    \multicolumn{2}{l|}{\textbf{Emphasized units}} &1305 \\
    \hline
    \multirow{4}{*}{\textbf{Utterances has X emphasized units}} & 1  &
    776\\
    &2  & 193 \\
    & 3 & 41 \\
    & 4 & 5 \\
    \end{tabular}

    \label{tbl:travel_corpus}
    \end{table}
    \end{center}
\section{Recording}
\label{sec:recording}

    \subsection{Recording Procedure}
    \label{sub:recording_procedure}
    The recording step required speakers who can speak both Japanese and English
    with good pronunciation and naturalness.

    For the digit corpus, a bilingual speaker was selected to speak
    each utterance. The speaker was asked to emphasize the indicated
    digit as if he was repeating the utterance for
    someone who misheard the digit in question.

    For the conversation corpus, as described in \cite{truong14ococosda},
    we selected 2 bilingual speakers, 1 Japanese and 1 American.
    We also have selected another 1 American bilingual, 6 monolingual
    Japanese, and 1 monolingual American speaker to make the corpus more
    general and has more speaker variety.
    The speakers were asked to read the text carefully, remember which words
    need to be emphasized, and emphasize them.

    The recording step was performed in a quiet environment.
    The audio was recorded with a frequency of 16 KHz, 16 bits, mono, little endian.
    After recording, all audio files were verified to ensure there is no clipping
    caused
    by the speaker speaking too loudly.

    Three sets of speech collected are shown in Tables
    \ref{tbl:small_speaker}  and  \ref{tbl:lvc_speaker}.

    \begin{center}
    \begin{table}[!ht]
     \centering
     \caption{Recorded speech data for the digit corpus}
    \begin{tabular}{l|l|l}
    \textbf{Speaker} & \textbf{Utterances} & \textbf{Emphasized words}\\
    \hline
    \textbf{Male American} & 500 & 500 \\

    \end{tabular}

    \label{tbl:small_speaker}
    \end{table}

    \begin{table}[!ht]
     \centering
     \caption{Recorded speech data for the conversation corpus}
    \begin{tabular}{l|l|l}
    \textbf{Speaker} & \textbf{Utterances} & \textbf{Emphasized words}\\
    \hline
    \textbf{Male Japanese} & 1015 & 1305 \\
    \hline
    \textbf{Male American} & 1015 & 1305 \\
    \end{tabular}

    \label{tbl:lvc_speaker}
    \end{table}
    \end{center}

    \subsection{Displayed Text}
    \label{sec:transciption}
    We prepared two sets of text for each language,
    one is plain text and the other is text with labels showing the
    emphasized units of the sentence.

    In the English text, the emphasized words are in upper case. Because in the
    Japanese text, as there is no notion of ``upper case,'' we instead choose to
    attach a marker ``\_em'' after the emphasized units.

    An example of the displayed text is shown in Table \ref{tbl:displayed_text}

    \begin{center}
    \begin{table}[!ht]
     \centering
     \caption{Transcription of the emphasized corpus}
    \begin{tabular}{c|c}
     \textbf{Language} & \textbf{Label}\\
     \hline
    \multirow{2}{*}{\textbf{Japanese}} &
    \begin{CJK}{UTF8}{min} カメラ\_emです. \end{CJK} \\
    & \begin{CJK}{UTF8}{min} ステキ\_em な
    日\_em ね. \end{CJK}\\
    \hline
    \multirow{2}{*}{\textbf{English}} & TODAY is really HOT. \\
     & BEAUTIFUL DAY, isn't it. \\
    \end{tabular}

    \label{tbl:displayed_text}
    \end{table}
    \end{center}


