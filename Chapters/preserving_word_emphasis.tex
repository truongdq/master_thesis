\chapter{Proposed Method for Preserving Word Emphasis} % Main chapter title
\label{preserving_word_emphasis} % For referencing the chapter elsewhere, use \ref{Chapter1}

  \section{Overview}
  \label{sec:preserving_word_emphasis_overview}
    In speech, emphasis is manifested by changing the duration, power, or $F_0$
    \cite{prosody_info}. The challenge in developing an S2S system that can
    accurately translate emphasis is that we must consider these acoustic features
    in three components: emphasis extraction, emphasis translation,
    and synthesis of emphasized speech.

    Figure~\ref{fig:es2s} illustrates the whole emphasis speech-to-speech translation system that
    this study proposed.
    With regards to emphasis modeling and estimation, this study proposes
    the idea of representing the word-level emphasis by a real-numbered value.
    This value is estimated for each word in an utterance by
    applying linear-regression hidden semi-Markov models (LR-HSMMs), which
    are a simple form of multi-regression hidden semi-Markov models (MR-HSMMs)
    \cite{Nose:2007:SCT:1522543.1522545}. We chose LR-HSMMs for our
    S2S model for two reasons. First, it allows us to build a single model for
    both emphasis estimation and synthesis of emphasized speech. Second, it is
    appropriate for tasks with words that do not exist in the training data, because it allows us to
    model speech at the phoneme level.

    The work on emphasis translation adopts conditional random fields (CRFs) \cite{Lafferty:2001:CRF:645530.655813}
    to translate the estimated emphasis sequence from a source language utterance into
    the emphasis sequence in the target language.
    CRFs allows us to flexibly integrate
    different features in the emphasis translation model.
    Finally, the text-to-speech
    system synthesizes emphasized speech using the translated text and the corresponding emphasis
    sequence.

    \begin{figure}
      \centering
      \includegraphics[width=12cm]{preserving_emphasis/emphasis_s2s}
      \caption{Emphasis speech-to-speech translation}
      \label{fig:es2s}
    \end{figure}


  \section{Stress and Emphasis in English and Japanese}
    \label{sec:stress_and_emphasis_in_english_and_japanese}
    Understanding the way people emphasize words in sentences is important to
    construct emphasized corpora as well as building the emphasized translation
    systems.  In this section, we describe the way people emphasize words in
    English and Japanese. Emphasis and stress have a close relationship, as they
    are both put a stronger sound on particular words. The main difference is that
    emphasis is generally intentional, when stress can be intentional or
    unintentional. In this work, we only take into account the intentional
    emphasis, so the emphasis and stress can be consider as one.

    \subsection{Emphasis in English}
    \label{sub:emphasis_in_english}
    English sentences typically contain at least one emphasized word.
    Essentially, the emphasized words depend on the question or the context,
    and they carry the new information \cite{emphasis}. Below are several
    examples of the emphasized words in English.

     \begin{itemize}
      \item It is \textbf{REALLY} hot today.
      \item \textbf{TODAY} is really hot.
      \item Do you have a \textbf{COMPUTER} ?
      \item It is \textbf{EXACTLY} the same as the original.
     \end{itemize}
    As seen above, the words \textbf{REALLY}, \textbf{HOT},
    \textbf{COMPUTER} and \textbf{EXACTLY} contain the new information, so they will
    be emphasized.
    These words can be nouns, adjectives, verbs or adverbs.
    However, the copula verbs are less often emphasized.

    \subsection{Emphasis in Japanese}
    \label{sub:emphasis_japanese}
    Emphasis in Japanese is different compared to English. Japanese speakers
    use more pauses before the emphasis words, less power and duration than English
    \cite{japan_prosodic}. The reason is that in Japanese, if we stretch out the
    duration of Japanese words too much, the meaning of them might change.

    In order to understand the way Japanese speakers emphasize words, we have
    conducted a survey to ask Japanese to answer a few questions that are
    related to the emphasis in Japanese. The survey contains a list of a short
    conversations,
    an example of which is shown below:
    \begin{center}
     \begin{CJK}{UTF8}{min} どこで食べますか？ - Where do you eat?　\\ Ans: 食堂で食べます - I eat
    in the cafeteria\end{CJK}
    \end{center}

    10 Japanese were selected to choose which words they will emphasize.
    The result showed that the emphasized words depend on the question,
    if the question is asked about the place, the emphasized words are noun,
    if the question is about the action, the emphasized words are verb and so on.
    All emphasized words convey new information in sentences.
    We also observed that the ``to be''
    verbs such as ``\begin{CJK}{UTF8}{min}です''\end{CJK} are seldom emphasized.

    The corpus construction will use this information to select
    words that need to be emphasized.

  \section{Emphasis Modeling}
  \label{sec:emphasis_modeling}
  In this section, we describe the use of linear regression hidden semi-Markov
  models (LR-HSMMs) in modeling word-level emphasis.
    \subsection{Linear-regression hidden semi-Markov model (LR-HSMM)}
    \label{sub:linear_regression_hidden_semi_markov_model_lr_hsmm_}
      We adopt LR-HSMMs, which are a simple form of MR-HSMMs \cite{Nose:2007:SCT:1522543.1522545} to
      model the emphasized speech as follows.
      We assume a word sequence consists of $J$ words $\w = [w_1, \cdots, w_j, \cdots,
      w_J]$, and a length $T$ vector sequence of acoustic features of the input utterance
      ${\vm{o}} = \left[\vm{o}^\top_1, \cdots,\vm{o}^\top_t
      \cdots, \vm{o}^\top_T\right]^\top$. As the observation feature vector
      $\vm{o}_t$ at frame $t$ we use a combination of the spectral feature vector
      $\vm{o}^{(1)}_t$ and the $F_0$ feature vector $\vm{o}^{(2)}_t$ as described in \cite{YoshimuraTMKK99}.
      The likelihood
      function of the LR-HSMMs is given by
      \begin{eqnarray}
        P\left(\vm{o} | \vm{\lambda}, \mathcal{M}\right)
        = \sum_{\mbox{all }\vm{q}} P\left(\vm{q} | \vm{\lambda}, \mathcal{M}\right)
        P\left(\vm{o} | \vm{q}, \vm{\lambda}, \mathcal{M}\right),
      \end{eqnarray}
      where $\vm{q} = \left\{q_1, \cdots, q_t, \cdots, q_T\right\}$ is the HSMM
      state sequence, $\vm{\lambda} = \left\{\lambda_1, \cdots, \lambda_j,
      \cdots, \lambda_J\right\}$ is the word-level emphasis weight sequence, and
      $\mathcal{M}$ is an HSMM parameter set. Note that in this paper, the
      emphasis weight is shared over all HSMM states corresponding to a word as
      shown in Fig. \ref{em_shared}.
      \begin{figure}[htb]
      \centering
      \centerline{\includegraphics[width=9.0cm]{preserving_emphasis/em_weight}}
      \caption{An example of word-level emphasis ($\lambda$). Each word has its own emphasis level,
      and the emphasis level of one word is shared among all the HMM states associated to that word.
      In this example, the word ``hot'' is emphasized, so it has higher emphasis level than the other words.
      }
      \label{em_shared}
      \end{figure}
      The state output probability density function modeled by a Gaussian
      distribution\footnote{Specifically, a multi-space probability distribution
          \cite{Tokuda:6495700} is used for the $F_0$ component in this
      paper.} is given by
      \begin{equation}
       P\left(\vm{o} | \vm{q}, \vm{\lambda}, \mathcal{M}\right) =
       \prod^T_{t=1} P\left(\vm{o}_t | q_t, \omega_t, \mathcal{M}\right),
      \end{equation}
      %\begin{equation}
       %P\left(\vm{o} | \vm{q}, \vm{\lambda}, \mathcal{M}\right) =
       %\prod^T_{t=1} P\left(\vm{o}_t | q_t, \lambda_t = \lambda_j, \mathcal{M}\right),
      %\end{equation}
      \begin{equation}
          P\left(\vm{o}_t | q_t = i, \omega_t, \mathcal{M}\right) \\
          = \prod^2_{s=1} \mathcal{N}\left(\vm{o}^{(s)}_t; \vm{\mu}^{(s)}_i + \omega_t \vm{b}^{(s)}_i, \vm{\Sigma}^{(s)}_i\right),
      \end{equation}
      %\begin{equation}
       %P\left(\vm{o}_t | q_t = i, \lambda_t = \lambda_j, \mathcal{M}\right) \\
       %= \prod^2_{s=1} \mathcal{N}\left(\vm{o}^{(s)}_t; \vm{\mu}^{(s)}_i + \lambda_t \vm{b}^{(s)}_i, \vm{\Sigma}^{(s)}_i\right),
      %\end{equation}
      where $\omega_t$ is frame-level emphasis equivalent to $\lambda_j$,
      where $j$ is the word corresponding to frame $t$,
      and $s$ is a stream index ({\em i.e.}, $s=1$ for the spectral feature and
      $s=2$ for the $F_0$ features).  At HSMM state $i$ for the
      $s^{\mbox{\footnotesize{th}}}$ stream, the mean vector is given by a linear
      combination of the vector $\vm{\mu}^{(s)}_i$ for normal speech and the
      vector $\vm{b}^{(s)}_i$ expressing the difference between normal speech and emphasized
      speech using $\omega_t$ as a weighting value, and the covariance matrix is
      $\vm{\Sigma}^{(s)}_i$.  Moreover, the duration probability is given by
      \begin{eqnarray}
       P\left(\vm{q} | \vm{\lambda}, \mathcal{M}\right)
       & = & \prod^N_{i=1} P\left(d_i | \omega_i, \mathcal{M}\right), \\
       P\left(d_i | \omega_i, \mathcal{M}\right)
       & = &
       \mathcal{N}\left(d_i; \mu^{(d)}_i + \omega_i b^{(d)}_i, {\sigma^{(d)}_i}^2\right),
      \end{eqnarray}
      where $\left\{d_1, \cdots, d_i, \cdots, d_N\right\}$ is a set of HSMM state
      durations corresponding to $\vm{q}$, $\omega_i = \lambda_j$ if
      $d_i \in w_j$, and $N$ is the number of states in the
      sentence HSMM sequence (i.e., the sum of $d_i$ over $N$ HSMM states is equivalent to
      $T$).  At HSMM state $i$, the mean of the Gaussian distribution is also
      given by a linear combination of the mean value $\mu^{(d)}_i$ for normal
      speech and the value $b^{(d)}_i$ expressing the difference between the normal speech and
      emphasized speech using $\omega_i$ as a weighting value, and the variance
      is given by ${\sigma^{(d)}_i}^2$.

      %The MR-HSMM flexibly models the multiple acoustic features in both normal
      %and emphasized speech by setting the emphasis weight.

    \subsection{LR-HSMM Training}
    \label{sub:lr_hsmm_training}
    The training process mainly follows the
    standard HMM-based speech synthesis training process
    \cite{Zen20091039,Yu2011914,5278371}.  First, the training data is
    labeled with full contextual factors encoding various features of the
    sentence.  To model emphasis, we use an additional contextual
    factor encoding the word-level emphasis by adding an emphasis question
    to the standard question set to cluster context-dependent phoneme
    HSMM states in each cluster \cite{5278371}.  Because of limitations in
    training data, we adopt decision-tree-based state tying
    \cite{Young:1994,2135925}. By adding the emphasis context to the full contextual
    label, the decision tree can also learn to partition the leaf nodes into
    two groups of normal and emphasized Gaussian as illustrated in Figure~\ref{fig:preserving_emphasis_dec_tree}.

    \begin{figure}[htpb]
      \centering
      \includegraphics[width=0.5\linewidth]{preserving_emphasis/dec_tree}
      \caption{An example of decision tree with emphasis question. The white dash nodes are
      emphasized nodes and white solid nodes are normal nodes.}
      \label{fig:preserving_emphasis_dec_tree}
    \end{figure}
    It is not guaranteed that
    all the full context labels have an emphasis Gaussian. However, as long as
    there is one full-context label of a word has an emphasis Gaussian, we can estimate
    the emphasis level for that word.
    The mean vectors of normal Gaussians are set to $\vm\mu^{(s)}_i$ and $\mu^{(d)}_i$, and the
    difference mean vectors between normal and emphasized Gaussians are set to
    $\vm b^{(s)}_i$ and $b^{(d)}_i$ so that the mean vectors of the LR-HSMMs
    are equal to those of emphasized Gaussians if the emphasis weight
    $\omega_i$ is set to 1. The covariance matrices and variances of the LR-HSMMs are
    set to those of normal Gaussians.

    \subsection{Emphasis Estimation}
    \label{sub:emphasis_estimation}
      Given an observation sequence $\vm{o} = \left[\vm{o}^\top_1, \cdots,
      \vm{o}^\top_t, \cdots, \vm{o}^\top_T\right]^\top$, and its transcription,
      the process to estimate the emphasis weight sequence is as
      follows: First, an LR-HSMM is constructed by selecting the Gaussian
      distributions corresponding to context of the given transcription.  Then,
      emphasis is estimated by determining maximum likelihood estimates of the emphasis weight sequence,
      which is the same as the adaptation process in the cluster adaptive
      training (CAT) algorithm \cite{848223}.  The
      word-level emphasis weight sequence is estimated by maximizing the HSMM
      likelihood as follows:
      \begin{eqnarray}
          \hat{\vm{\lambda}} = \arg\max_{\fvm{\lambda}}
          P\left(\vm{o} | \vm{\lambda}, \mathcal{M}\right).
      \end{eqnarray}
      This maximization process is performed with the EM algorithm \cite{maximumlikelihood}.
      In the E-step, posterior probabilities are calculated as follows:
      \begin{eqnarray}
          \gamma^{(s)}_{i,t} & = & P(q_t = i | \vm{o}, \vm{\lambda}, \mathcal{M}), \\
          \gamma^{(d)}_{i,t} & = & P(d_i = t | \vm{o}, \vm{\lambda}, \mathcal{M}).
      \end{eqnarray}
      Then, in the M-step, the maximum likelihood estimate of the word-level
      emphasis weight sequence $\hat{\vm{\lambda}} = \left\{\hat{\lambda}_1,
          \cdots, \hat{\lambda}_j, \cdots, \hat{\lambda}_J\right\}$ is determined as
      \begin{eqnarray}
          \hat{\lambda}_j & = & g^{-1}_j k_j,
      \end{eqnarray}
      where $g_j$ and $k_j$ are calculated by

      \begin{eqnarray}
          g_j & = &
          \sum_{i\in q(j)} \left[ \sum^2_{s=1} \sum^T_{t=1}
              \gamma^{(s)}_{i,t} {\vm{b}^{(s)}_i}^\top {\vm{\Sigma}^{(s)}_i}^{-1} \vm{b}^{(s)}_i
              + \sum^T_{t=1} \gamma^{(d)}_{i,t} {b^{(d)}_i}^2 {\sigma^{(d)}_i}^{-2}
          \right],
          \label{cal_g}
          \\
          k_j & = &
          \sum_{i\in q(j)} \left[ \sum^2_{s=1}
              {\vm{b}^{(s)}_i}^\top {\vm{\Sigma}^{(s)}_i}^{-1}
              \sum^T_{t=1} \gamma^{(s)}_{i,t} \left(\vm{o}^{(s)}_t - \vm{\mu}^{(s)}_i\right)
              \right. \nonumber \\
              & & \left. + b^{(d)}_i {\sigma^{(d)}_i}^{-2}
          \sum^T_{t=1} \gamma^{(d)}_{i,t} \left(d_t - \mu^{(d)}_i\right) \right],
          \label{cal_k}
      \end{eqnarray}
      where $q(j)$ indicates a set of HSMM states corresponding to word $w_j$.  It
      should be noted that in this framework we can easily control the effect of
      individual acoustic features on emphasis estimation by selecting Gaussian
      components used in the M-step as shown in Eqs. \ref{cal_g} and \ref{cal_k}.
      %controlling the range of the stream index $s$.

  \section{Emphasis Translation}
  \label{sec:emphasis_translation}
    %Our proposed emphasized speech translation model consists of a conventional
    %S2S system, an emphasis estimation model, and an emphasis translation model as illustrated
    %in Figure. \ref{fig:es2s}.
    %In this paper, we focus on the emphasis modeling and translation. Therefore,
    %we assume that the ASR and MT systems provide correct transcriptions and translation outputs.
    \subsection{Conditional Random Field}
    \label{sub:crf_des}
    Conditional random fields (CRF) \cite{Lafferty:2001:CRF:645530.655813} are discriminative models which model the
    conditional probability $P(\y|\x)$ directly instead of the join probability
    $P(\y, \x)$ (also known as the generative model). The advantage of the
    conditional model is that we do not have to model the $P(\x)$ which can
    include complex dependencies.  This leads to much simpler and compact model
    than the generative approach with the ability to incorporating a large
    number of input feature $\x$.

    The linear-chain CRF can be depict as a undirected graph in Figure~\ref{fig:crf_illustration}.
    $\theta$ and $\mu$ are equivalent to the transition probability and emission probabilities in
    the hidden Markov model.
    \begin{figure}[htpb]
      \centering
      \includegraphics[width=6cm]{preserving_emphasis/crf_illustration}
      \caption{The linear-chain conditional random field with the model parameters \{$\theta$, $\mu$\}.}
      \label{fig:crf_illustration}
    \end{figure}
    Given a training data that consists of $T$ samples $D = [(\x_1, y_1), \cdots, (\x_t, y_t), \cdots, (\x_T, y_T)]$,
    the conditional probability is calculated as,
      \begin{equation}
        P(\y | \x) = \frac{1}{Z(\x)} \sum_{t=1}^T
            \exp \left \{
              \sum_{i,j \in S} \theta_{ij} \mathbf{1}_{\{y_t=i\}} \mathbf{1}_{\{y_{t-1} = j\}}
                   +
                   \sum_{i \in S} \sum_{o \in O} \mu_{oi} \mathbf{1}_{\{y_t = i\}} \mathbf{1}_{\{x_t=0\}}
                 \right \}
        %P(\y | \x) = \frac{1}{Z(\x)} \prod_{t=1}^T \exp \left \{ \sum_{k=1}^K \theta_k f_k(y_t, y_{t-1}, \x_t) \right \},
          \label{eqn:crf_general}
      \end{equation}
      where $\{S, O\}$ are the state and observation space, $\{i, j, o\}$ denote the states and observation, respectively.

      We can write the Equation \ref{eqn:crf_general} more compactly by using the concept of feature function
      $f_{ij}(y, y', \x) = \mathbf{1}_{\{y=i\}}\mathbf{1}_{\{y'=j\}}$ and
      $f_{io}(y, y', x) = \mathbf{1}_{\{y=i\}}\mathbf{1}_{\{x=o\}}$. The annotation $\theta_k$ and $f_k$ are used for the general
      model parameter $\theta_{ij}$ and $\mu_{io}$ and the feature function that ranges over both $f_{ij}$ and $f_{io}$. Then the Equation \ref{eqn:crf_general} becomes
      \begin{equation}
        P(\y | \x) = \frac{1}{Z(\x)} \prod_{t=1}^T \exp \left \{ \sum_{k=1}^K \theta_k f_k(y_t, y_{t-1}, \x_t) \right \},
          \label{eqn:crf_linear}
      \end{equation}
    where $Z(\x)$ is a normalization function
    \begin{equation}
      Z(\x) = \sum_{\y'} \prod_{t=1}^T \exp \left \{ \sum_{k=1}^K \theta_k f_k(y'_t, y'_{t-1}, \x_t) \right \}
    \end{equation}

    The model parameter $\theta$ are optimized by maximizing the conditional probability
    \begin{equation}
      \mathcal{L}({\theta}) =  P(\y | \x).
    \end{equation}
    The simplest way to optimize $\mathcal{L}(\theta)$ is gradient descent, but the convergence speed is slow \cite{sutton2006introduction}.
    In practice, the common optimization approach is limited-memory Broyden-Fletcher-Goldfarb-Shanno (L-BFGS) \cite{citeulike:1284223}.

    \subsection{Emphasis Translation with Conditional Random Fields}
    \label{sub:conditional_random_fields}
      Our next step is emphasis translation, or to take word-level emphasis estimates
      in the source languages $\vm {\hat \lambda}^{(f)}$, and convert them
      to emphasis estimates in the target language $\vm{\hat \lambda}^{(e)}$.
      We perform estimation of emphasis in the target language using the conditional
      random fields (CRFs) approach. To train CRFs to predict target
      side emphasis, we create training data consisting of source and target
      words $\w^{(f)}$ and $\w^{(e)}$, and the corresponding estimated emphasis values.
      As $\vm {\hat \lambda}^{(e)}$ is a sequence of continuous values, and CRFs requires
      discrete state sequences, we first quantize $\vm {\hat \lambda}^{(f)}$
      and $\vm {\hat \lambda}^{(e)}$ into buckets, giving us a discrete sequence
      $\vm {\hat \lambda}^{(f)'}$ and $\vm {\hat \lambda}^{(e)'}$. We then create CRFs
      training data that consists of $N$ samples $D = [(\x_1, \lambda_1^{(e)'}), \cdots, (\x_n, \lambda_n^{(e)'}) , \cdots, (\x_N, \lambda_N^{(e)'})]$,
      where $\x_n$ is a feature vector for each word in $w_n^{(e)}$ consisting of:
        \begin{itemize}
            \setlength\itemsep{0em}
            \item source word-level emphasis $\lambda_j^{(f)}$, and its context,
            \item source word $w_j^{(f)}$, and word context,
            \item source word part of speech (PoS) $pos(w_j^{(f)})$, and PoS context,
            \item target word $w_n^{(e)}$, and word context,
            \item target word PoS $pos(w_n^{(e)})$, and PoS context,
        \end{itemize}
        where context means the information of one succeeding and one preceding words.

        To decide which source features correspond to a target word $w_n^{(e)}$, we use
        one-to-one word alignments between $w_j^{(f)}$ and $w_n^{(e)}$.
      The CRFs model parameters are optimized using Limited-memory
      Broyden-Fletcher-Goldfarb-Shanno (L-BFGS) approach
      \cite{citeulike:1284223} as implemented in CRFSuite \cite{CRFsuite}.

    %\subsection{Pause prediction}
    %\label{sub:pause_prediction
