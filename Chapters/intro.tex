% Introduction

\chapter{Introduction} % Main chapter title
\label{Introduction} % For referencing the chapter elsewhere, use \ref{Chapter1}

%----------------------------------------------------------------------------------------
  \section{Multilingual Communication and Paralinguistic Information}
  \label{sub:multilingual_communication}
  Speech is a very powerful communication channel for mankind. It is produced
  by a complex mechanism. The electric signal comes from the brain to control the
  movement of the vocal cord to vibrate with a specific frequency. The sound
  goes though the vocal tract to the mouth, nose and is converted to
  speech. That complex collaboration creates a powerful way to
  express the idea, and brings people closer together.

  The nice thing about speech is that it not only expresses the idea of the speaker,
  but also conveys paralinguistic information such as emotion and emphasis as well.
  This information helps us not only to understand each other, but
  also be aware of feelings, or understand traits as well. For
  example, let's think about a phone call between a user and an operator as illustrated
  in Figure~\ref{fig:phone_call_example}.
  \begin{figure}[htpb]
    \centering
    \includegraphics[width=7cm]{introduction/example_phone_call}
    \caption{An example of a misheard situation where people put more focus on the important words.}
    \label{fig:phone_call_example}
  \end{figure}
  The operator asked the user for a phone number. But some noise in the
  transmission line causes some words are misheard. The user has to repeat the
  speech one more time. But this time, he puts more focus on particular words
  that are misheard before. In another case, when he has to repeat his speech
  many times, he will get angry. The angry speech is also very different from
  normal speech. All these case are very typical in human
  communication in daily life. Without paralinguistic information, the human
  communication would be very flat.

  %Nowadays, with the rapid development of the Internet, people can communicate
  %without the distance. It is very easy to connect and talk with other people
  %across countries. . the biggest obstacle that prevents the human
  %communication is the variety of the languages.

  On the other hand, there is not only one language over the world, and it is also
  not easy to understand the speech of a foreign people. According to
  Ethnologue \footnote{http://www.ethnologue.com/} there are about 7,106 spoken
  languages over the planet as of 2014. Although this number is approximate, it
  showed us the diversity of the languages. The needs of
  understanding eachother regardless of language is increasing faster than ever.
  However it is impossible to learn all languages, and might even harder
  to understand the emotion of the people who speak in different language due
  to cultural differences.
  Paralinguistics also play an important role in cross-lingual communication.
  For example, in a cross-lingual meeting with an interpreter, if paralinguistic
  information
  is not translated, the communication will be boring, and also the idea might not
  be fully spread across interlocutors.

  \section{Breaking Language Barrier with Speech-to-Speech Translation}
  \label{sub:breaking_language_barrier_with_speech_to_speech_translation}
  In order to break down the language barrier, speech-to-speech translation
  \cite{s2s_survey} techniques have been studied and developed for many years.
  It translates the speech across languages by
  the combination of three components: the automatic speech
  recognition, which converts speech into text; machine translation,
  which translates text across languages; and finally, speech synthesis,
  which synthesizes speech from the translated text.

  The development of the speech translation technologies has been growing very
  rapidly.  Since the first version was developed in 1980s with very limited
  vocabularies and simple rule-based translation, until now with very large
  vocabularies and continuous simultaneous speech translation \cite{s2s,
  shimizu13iwslt}. Some big companies have also launched the speech
  translation technologies into their services.
  We can talk to some extent with other people in different language without
  learning that language.
  However, there are still problems remaining, which will be described in the
  next section.


  \section{Problems and Related Works}
  \label{sub:problems_and_limitations}
  Let's go back to the importance of the paralinguistic information.
  The Figure~\ref{fig:phone_call_example} shows an example of a misheard
  situation where the emphasis information helps to convey the meaning
  of speaker. That kind of situation is more likely to happen in cross-lingual
  speech communication using a S2S system. The S2S system can
  translate the meaning of the sentence to a target language.
  However, it discards the importance of
  paralinguistic information. Regardless of how emotional or emphasized in the source
  speech is, the output target speech will be always expressed in the same way.

  There are some previous studies tried to tackle problem. \cite{kano2013}
  translated emphasis information by extracting the emphasis parameter in
  speech signal and translate it into the target language by using a neural
  network model.  The problem of this work is that the emphasis parameters
  extraction did not consider all the acoustic information such as the
  duration, the power and the fundamental frequency ($F0$).  The neural network
  also required a lot of training data to be robust. The domain is also very
  limited, the 10 digits.  Another work in \cite{prosody_gen} tried
  to translate the fundamental frequency ($F0$) by extracting and classifying the $F0$ for each
  accent group, then using an accent group mapping function to
  find the corresponding accent group in the target language. However, the
  mapping function is fixed, so there will be no level control of the
  translation.

  \section{Contribution}
  \label{sub:contribution}
  As described in the previous section, the paralinguistic information consists
  of many factors such as emphasis and emotion. Because it is hard to handle all
  of them at the same time, this thesis focuses on handling emphasis
  information as the first step. In order to address the problem of the
  conventional S2S systems, this thesis has collected a new emphasis corpus and
  introduced two additional components to the conventional S2S system, emphasis
  estimation and emphasis translation as follows,
  \begin{description}
    \item[Corpus collection:] The previous study \cite{kano2013} worked on
      very limited data consists of 10 digit numbers. This thesis has collected a
      new database that expresses emphasis in a more natural way and
      with larger vocabularies than the previous work.
    \item[Emphasis modeling:] This study introduced a way to measure emphasis
      in speech called word-level emphasis degree. The idea is to estimate
      a real-numbered value for every word in an utterance that represents for emphasis of
      that word. The words that
      are emphasized will have a higher value compared to the other words. An example
      is shown in Figure~\ref{fig:egs_emphasis}.
    \item[Emphasis translation:] This study adopt conditional random fields (CRFs) to
      translate the word-level emphasis across languages. End-to-end
      emphasized speech translation experiments are also conducted to evaluate
      the translation performance.
  \end{description}

  \begin{figure}[ht]
    \centering
    \includegraphics[width=6cm]{introduction/emphasis_example}
    \caption{An example of Word-level emphasis. In this example, the word ``very'' and ``hot''
    are emphasized with higher emphasis value.}
    \label{fig:egs_emphasis}
  \end{figure}
%----------------------------------------------------------------------------------------
  \section{Outline of the Thesis}
  The thesis is structured as follows.
  Chapter 2 gives an overview of the S2S system, and describes how it can
  translate the speech across languages. The three main components of the S2S
  system, speech recognition (ASR), machine translation (MT), and
  text-to-speech synthesis (TTS) are also described. This chapter points out that in the
  conventional speech translation system, the ASR system only recognize the
  text representing the meaning of the speech, and the MT and TTS only deal
  with the text as well. This is the main reason why the conventional S2S system can
  not translate the paralinguistic information.

  With regards to the emphasis modeling and translation, Chapter 3 presents the use
  of linear regression hidden semi-Markov models (LR-HSMM) to model and estimate a
  real-numbered value that represents for word-level emphasis. The basic idea is
  that we model the emphasized speech and normal speech models separately, and the
  word-level emphasis is an interpolation parameter between those two models. If
  the words are emphasized, they will have a higher emphasis level than the other
  words. This chapter also describes the use of conditional random fields (CRFs)
  as the emphasis translation model. The advantage is that we can easily add
  more features as input for the translation model to achive the best accuracy.

  In order to develop a scalable emphasis translation system, Chapter 4
  presents the emphasized speech corpus collection procedure. The new collected
  corpus expresses the emphasis in a more natural way and also larger
  vocabularies. The corpus is not only useful for emphasized speech analysis purposes,
  but also for developing the emphasis translation system.

  Chapter 5 presents the experiments on the collected emphasis corpus, emphasis
  modeling, as well as emphasis translation. The analyses on the emphasis
  corpus show the difference in the way people emphasise words in different
  languages. The result indicates that Japanese subjects use less duration that
  English subjects, because if the words in Japanese are stretched out too much,
  the meaning might change completely. Experiments on emphasis modeling
  indicates the efficiency of LR-HSMM in modeling and estimating word-level
  emphasis when using all the speech feature including spectral, log $F0$, and
  duration. With regards to emphasis translation, experiments show that the
  CRF model is able to translate the emphasis information accurately while
  preserving the naturalness of the audio.

  Chapter 6 concludes the thesis with an overview of emphasis translation and
  the direction for the future research.

